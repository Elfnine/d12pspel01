package  
{

	public class Resources 
	{
		[Embed(source = "../lib/graphics/player.png")]
		public static var imgPlayer:Class;
		
		[Embed(source = "../lib/graphics/playerAnimation.png")]
		public static var imgPlayerAnimation:Class;
		
		[Embed(source = "../lib/graphics/enemy.png")]
		public static var imgEnemy:Class;
	}

}