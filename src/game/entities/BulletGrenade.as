package game.entities 
{
	import game.entities.weapons.Weapon;
	import net.flashpunk.graphics.Image;

	public class BulletGrenade extends Bullet
	{
		
		public function BulletGrenade(_weapon:Weapon, _x:int, _y:int, _dirX:Number, _dirY:Number) 
		{
			super(_weapon, _x, _y, _dirX, _dirY);
			
			graphic = Image.createRect(14, 14, 0x0000FF);
			graphic.x -= 7;
			graphic.y -= 7;
			
			setHitbox(14, 14, 7, 7);
		}
		
		override public function remove():void
		{
			for (var i:int = 0; i < 16; i++) {
			/*
			// Skit/fyrkant spridning
			var b:Bullet = new Bullet(weapon, x, y, -1 + 2 * Math.random(), -1 + 2 * Math.random());
			*/
			
			// Cirkel spridning
			var a:Number = i / 16 * Math.PI * 2;
			var b:Bullet = new Bullet(weapon, x, y, Math.cos(a), Math.sin(a));
			
			b.speed = 6;
			
			weapon.owner.world.add(b);
			}
			
			super.remove();
		}
		
	}

}