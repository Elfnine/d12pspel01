package game.entities 
{
	import game.entities.weapons.Weapon;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;

	public class Bullet extends Entity
	{
		public var weapon:Weapon;
		
		public var speed:Number = 10;
		
		public var dirX:Number;
		public var dirY:Number;
		
		public var friction:Number = 0.96;
		
		public var damage:Number = 1;
		
		public function Bullet(_weapon:Weapon, _x:int, _y:int, _dirX:Number, _dirY:Number) 
		{
			super(_x, _y);
			
			weapon = _weapon;
			
			dirX = _dirX;
			dirY = _dirY;
			
			graphic = Image.createRect(6, 6, 0x0000FF);
			graphic.x -= 3;
			graphic.y -= 3;
			
			setHitbox(6, 6, 3, 3);
		}
		
		override public function update():void
		{
			x += dirX * speed;
			y += dirY * speed;
			speed *= friction;
			
			if (speed <= 2) {
				remove();
			}
			
			var hitEnemy:Enemy = collide("enemy", x, y) as Enemy;
			if (hitEnemy != null) {
				remove();
				hitEnemy.health -= damage;
			}
			
		}
		
		public function remove():void
		{
			world.remove(this);
		}
		
	}

}