package game.entities 
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;

	public class Enemy extends Entity
	{
		public var alive:Boolean = true;
		
		private var _health:Number = 1;
		
		public var speed:Number = 3;
		public var vx:Number = 0;
		public var vy:Number = 0;
		
		public function Enemy()
		{
			super(0, 0);
			
			graphic = new Image(Resources.imgEnemy);
			graphic.x = -10;
			graphic.y = -10;
			
			setHitbox(20, 20, 10, 10);
			
			type = "enemy";
			
			setSpawnPosition();
			
			// Koll så att int vi krockar i nan ader enemy
			// Så att vi fastnar
			// Ifall vi gör he så lägg ny position
			while (tooCloseToEnemy()) {
				setSpawnPosition();
			}
		}
		
		override public function update():void
		{
			vx = 0;
			vy = 0;
			
			if (x < Globals.gameWorld.player.x) {
				vx += speed;
			}
			else if (x > Globals.gameWorld.player.x) {
				vx -= speed;
			}
			if (y < Globals.gameWorld.player.y) {
				vy += speed;
			}
			else if (y > Globals.gameWorld.player.y) {
				vy -= speed;
			}
			
			moveBy(vx, vy, ["player", "enemy"]);
		}
		
		public function get health():Number
		{
			return _health;
		}
		
		public function set health(value:Number):void
		{
			_health = value;
			
			if (_health <= 0) {
				die();
			}
		}
		
		public function die():void
		{
			if (alive) {
				alive = false;
				Globals.gameWorld.removeEnemy(this);
			}
		}
		
		private function setSpawnPosition():void
		{
			x = -20;
			y = 480 * Math.random();
			var r:Number = Math.random();
			
			if (r < 0.25) {
				x = 640 + 20;
			} else if (r < 0.5) {
				y = -20;
				x = 640 * Math.random();
			} else if (r < 0.75) {
				y = 480 + 20;
				x = 640 * Math.random();
			}
		}
		
		private function tooCloseToEnemy():Boolean
		{
			for (var i:int = Globals.gameWorld.enemies.length - 1; i >= 0; i--)
			{
				if (FP.distance(x, y, Globals.gameWorld.enemies[i].x, Globals.gameWorld.enemies[i].y) < 20) 
				{
					return true;
				}
			}
			return false;
			
		}
		
	}

}