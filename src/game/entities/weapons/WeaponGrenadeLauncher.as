package game.entities.weapons 
{
	import game.entities.Bullet;
	import game.entities.BulletGrenade;
	import net.flashpunk.Entity;

	public class WeaponGrenadeLauncher extends Weapon
	{
		
		public function WeaponGrenadeLauncher(_owner:Entity, _evHandleShooting:Function = null) 
		{
			super(_owner, _evHandleShooting);
			
			cooldown = 30;
		}
		
		override public function shoot(dirX:Number, dirY:Number):void
		{
			// Lägger till skott till världen!
			owner.world.add(new BulletGrenade(this, owner.x, owner.y, dirX, dirY));
			
			super.shoot(dirX, dirY);
		}
		
	}

}