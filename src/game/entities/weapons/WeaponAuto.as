package game.entities.weapons 
{
	import game.entities.Bullet;
	import net.flashpunk.Entity;

	public class WeaponAuto extends Weapon
	{
		
		public function WeaponAuto(_owner:Entity, _evHandleShooting:Function = null) 
		{
			super(_owner, _evHandleShooting);
			
			cooldown = 2;
		}
		
		override public function shoot(dirX:Number, dirY:Number):void
		{
			// Lägger till skott till världen!
			var b:Bullet = new Bullet(this, owner.x, owner.y, dirX - 0.2 + 0.4 * Math.random(), dirY - 0.2 + 0.4 * Math.random());
			
			b.damage = 0.5;
			b.friction = 0.96;
			
			owner.world.add(b);
			
			super.shoot(dirX, dirY);
		}
		
	}

}