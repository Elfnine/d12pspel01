package game.entities.weapons 
{
	import game.entities.Bullet;
	import net.flashpunk.Entity;

	public class WeaponSmg extends Weapon
	{
		
		public function WeaponSmg(_owner:Entity, _evHandleShooting:Function = null) 
		{
			super(_owner, _evHandleShooting);
			
			cooldown = 1;
		}
		
		override public function shoot(dirX:Number, dirY:Number):void
		{
			// Lägger till skott till världen!
			var b:Bullet = new Bullet(this, owner.x, owner.y, dirX - 0.1 + 0.2 * Math.random(), dirY - 0.1 + 0.2 * Math.random());
			
			b.damage = 0.25;
			b.friction = 0.96;
			
			owner.world.add(b);
			
			super.shoot(dirX, dirY);
		}
		
	}

}